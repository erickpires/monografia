Espera	MACRO               ; // Duracao da espera: 7 * T + 3 ciclos de relogio
        LOCAL	Esp, FimEsp
Esp:	SB	Zero            ; Enquanto ( T )	// 0 - T, pede se T != 0
        DNP	FimEsp
        SB	HFFFF           ;   T = -1 - ( 0 - T );	// T = T - 1, nunca ha pede
        DNP	Esp
FimEsp:
        ENDM
