      ORG 0
Zero: DW 0
Um:   DW 1


      IniRam equ 400 h

TROCA MACRO	OP0,OP1,TMP	; Troca os valores de OP0 e OP1 usando variavel TMP
      LM	OP0         ; T recebe o valor original de OP0, nao altera o pede
      EM	TMP         ; TMP = OP0
      LM	OP1
      EM	OP0         ; OP0 = OP1
      LM	TMP
      EM	OP1         ; OP1 = TMP
      ENDM

      ORG 15

      Var1 equ IniRam
      Var2 equ (Var1 + 1)
      Var3 equ (Var2 + 1)

      LM Zero
      EM Var1
      LM Um
      EM Var2
      TROCA Var1, Var2, Var3    ; Var1 = 1, Var2 = 0
