\chapter{UM MONTADOR EXTREMAMENTE SIMPLES}
\label{chp:mes}

\section{Sobre o montador}

O montador (do inglês \emph{assembler}) é a ferramenta responsável por transformar código em linguagem de montagem (\emph{assembly}) em código de máquina (codificação binária) que o computador pode de fato executar. Essa transformação é em geral um-para-um, ou seja, cada linha em um programa em linguagem de montagem corresponde a uma única instrução, e, de maneira geral, a uma  quantidade fixa de \emph{bytes}, no programa em linguagem de máquina. Podemos dizer que a linguagem de montagem representa o mais próximo possível a linguagem de máquina, mas garantindo que o programa possa ser facilmente entendido pelo programador.

Ser capaz de transformar linguagem de montagem em linguagem de máquina de forma automática é algo desejável para qualquer computador, incluindo o CES. Essa transformação pode ser feita de maneira manual, dada a simples correlação entre as duas linguagens, mas a transformação manual de um programa sem que ocorram erros, além de demandar muito tempo, é bastante inconveniente.

Existem montadores com muitas funcionalidades além de simplesmente transformar linguagem de montagem em linguagem de máquina, um exemplo é o montador da arquitetura \emph{x86}. Uma possível abordagem para gerar código de máquina para o CES é a utilização funcionalidades de montadores mais complexos de outra arquitetura. Porém, essa solução alternativa é um pouco complicada e sujeita a erros. Por esse motivo um montador capaz de transformar a linguagem de montagem do CES em código de máquina foi desenvolvido e nomeado Montador Extremamente Simples (MES). O código do projeto pode ser encontrado em: \cite{url:MES}.

O objetivo do MES é ser um montador simples para o CES, porém, é necessário ter o mínimo de funcionalidades que se espera em um montador. O Código \ref{cod:asm1} é um exemplo típico em linguagem de montagem que ajuda a entender quais são essas funcionalidades básicas.

O MES utiliza os seguintes mnemônicos para as instruções do CES:

\begin{itemize}
\item $LM$: Lê valor da memória;
\item $EM$: Escreve valor na memória;
\item $SB$: Subtrai;
\item $DNP$: Desvia se não pede emprestado.
\end{itemize}

Contrário ao comportamento de alguns montadores, o MES é sensível à caixa das letras, ou seja, ele diferencia letras maiúsculas das minúsculas.

\newpage

\lstinputlisting[style=customCES,caption={Exemplo de código de montagem},label=cod:asm1]{codigos/Exemplo_Assembly.asm}

\newpage

Examinando o Código \ref{cod:asm1}, podemos notar algumas funcionalidades esperadas de um montador que não estão diretamente relacionadas com transformação de código em linguagem de montagem em código de máquina. Algumas delas são:

\begin{itemize}

\item $equ$: Comando usado para criar substituições por um determinado valor. Uma substituição é declarada inserindo o nome a ser substituído, seguindo da palavra reservada $equ$, seguida do valor da substituição. Durante o processamento do código, qualquer ocorrência  dessas palavras será substituída pelo seu respectivo valor. Vale notar que esta substituição é textual e ocorre em todo o arquivo (assim como $\#define$ na linguagem C, por exemplo), ou seja, não existe escopo para essas substituições;

\item \emph{Macros}: Macros são trechos de código que não fazem parte do programa por si só. Em vez disso, sempre que o nome da macro aparece no código original esse é substituído durante o processamento pelo trecho de código correspondente à macro. Macros são iniciadas com o nome da macro seguido pela palavra reservada $MACRO$. Ao lado direito pode vir a lista de  argumentos. Durante a invocação de uma macro, uma lista de parâmetros com o mesmo comprimento da lista de argumentos deve ser passada. Os argumentos serão substituídos em ordem pelos seus respectivos valores (parâmetros). Um exemplo de uma macro que recebe parâmetros, assim como a sua invocação, pode ser encontrado no Código \ref{cod:asm2}.

  Uma macro pode possuir rótulos locais e globais, como descrito no item \emph{Labels} abaixo.

  Uma macro é terminada pela palavra reservada $ENDM$;

\item $DW$: Alocação de constantes (ou variáveis). Instrui o montador a utilizar o endereço atual de memória, guardando nesta posição o valor que está à direita da palavra reservada $DW$. No código de exemplo, as oito linhas no início alocam constantes nos oito primeiros endereços do programa;

\item \emph{Rótulos}:  Rótulos (\emph{labels}) são palavras utilizadas para representar (e abstrair) o endereço ou de uma instrução ou de um dado alocado com $DW$. A declaração de um rótulo é realizada iniciando uma determinada linha com o nome desejado para o rótulo e inserindo dois-pontos ($:$) logo em seguida. O nome dos rótulos pode ser utilizado em qualquer lugar em que o valor de um endereço pode ser usado, ou seja, como operando para uma instrução ou como parâmetro para uma macro. Vale notar que embora as pseudo-instruções \emph{equ} e \emph{MACRO} possuam um nome no início da linha, esses nomes não são rótulos e por isso não utilizam dois-pontos.

  Rótulos declarados dentro de uma macro são globais, isto é, pertencem ao escopo do programa inteiro. Na maioria dos casos deseja-se um escopo local para rótulos declarados em uma macro, e por isso existe a palavra reservada $LOCAL$, que deve ser inserida na primeira linha após a declaração da macro. Seguindo esta palavra reservada, deve vir uma lista com o nome dos rótulos que serão declarados na macro e que terão escopo local. Um exemplo disso pode ser visto no Código \ref{cod:asm3};

\item $\$$: O caracter $\$$ é um rótulo especial que é sempre substituído pelo endereço da posição atual de memória. No código de exemplo ele é utilizado para fazer com que uma instrução desvie para si mesma;

\item \emph{Valores em hexadecimal}: A habilidade de expressar um valor em hexadecimal utilizando a letra \emph{h} como sufixo do número. Por exemplo, \emph{8000h} representa o valor $32768$. Uma limitação do montador é a de que valores numéricos precisam começar com caracteres entre 0 e 9. Por este motivo, é necessário iniciar um número com o dígito 0 se o seu dígito mais significativo for maior do que 9. No código de exemplo, podemos ver isso acontecer com o valor \emph{0FFFFh};

\item $ORG$: Origem. Comando que instrui o montador sobre qual endereço deve ser utilizado como base para as alocações de endereço. No código de exemplo, $ORG$ $15$ significa que todas as instruções abaixo dessa linha devem começar no endereço $15$;

\item \emph{Expressões matemáticas}: O valor dos operandos das instruções pode ser gerado a partir da avaliação de uma expressão matemática. As expressões podem conter as quatro operações básicas (soma, subtração, multiplicação e divisão), sendo que as operações de multiplicação e divisão têm precedência sobre as operações de soma e subtração (como esperado). Além disso, é possível utilizar parênteses para manipular a ordem em que as operações são avaliadas. As expressões podem envolver rótulos e substituições declaradas com o comando $equ$. No código de exemplo, podemos notar o uso de uma expressão na declaração da substituição $Var1$.

\item \emph{Inclusões}: Um arquivo pode incluir outros arquivos. Para incluir outro arquivo, é utilizado a diretiva $\#include\ nome\_do\_arquivo$. O arquivo incluído é lido e inserido no local da inclusão.

\end{itemize}

\newpage

\lstinputlisting[style=customCES,caption={Exemplo de macro},label=cod:asm3]{codigos/Exemplo_Macro_Local.asm}

\lstinputlisting[style=customCES,caption={Exemplo de macro},label=cod:asm2]{codigos/Exemplo_Macro_Args.asm}

\section{Interface da linha de comando}

O MES é um programa de linha de comando, e seu uso é bem simples. A seguir está a especificação de como o comando deve ser chamado.

\[mes <arquivo.asm>\ [-h]\ [-H]\ [-c]\ [-B]\ [-o <prefixo\_saida>] \]

Os parâmetros entre colchetes são opcionais. O MES também possui diferentes métodos de saída, que podem gerar mais de um arquivo. Todos esses arquivos possuem o mesmo prefixo, alterando apenas a extensão. Por padrão, esse prefixo é obtido a partir do arquivo de entrada, removendo a sua extensão.

Os parâmetros opcionais tem o seguinte significado:

\begin{itemize}

\item $-h$: Imprimir ajuda. Quando este parâmetro está present, o montador imprime uma mensagem de ajuda e termina a execução;

\item $-H$: \emph{Intel Hexadecimal}. Quando este parâmetro está presente, o montador emite um arquivo de extensão $.hex$ com o padrão Intel (como descrito em \cite{url:intel_hex});

\item $-c$: Arquivo hexadecimal do CES. Quando este parâmetro está presente, o montador emite um arquivo de extensão $.ces.hex$ que segue o padrão do arquivo aceito no simulador do CES (\cite{url:SimCES});

\item $-B$: Binários separados. Quando este parâmetro está presente, o montador emite dois arquivos de extensões $.1.bin$ e $.2.bin$. O primeiro arquivo contém os \emph{bytes} menos significativos das palavras de 16 \emph{bits} do programa, enquanto que o segundo arquivo contém os \emph{bytes} mais significativos. Este modo de saída é útil para gerar arquivos que serão utilizados na implementação do CES, uma vez que foram utilizadas duas ROMs de 8 \emph{bits} de largura cada;

\item $-o <prefixo\_saida>$: Saída. Faz com que o prefixo dos arquivos de saída tenha o valor $prefixo\_saida$.

\end{itemize}

Além dos tipos de saída que podem ser habilitados com os parâmetros descritos acima, o MES sempre gera dois arquivos de saída.

\begin{itemize}

\item Arquivo binário: Um único arquivo binário (de extensão \emph{.bin}) contendo o código de máquina do programa;

\item Arquivo de listagem: É um arquivo de extensão \emph{.lst} que contém o código de montagem final que foi utilizado para gerar o código de máquina, ou seja, com todas as macros e rótulos substituídos;

\end{itemize}

\section{Operação do montador}

Para montar o programa recebido, o MES executa diversas passagens pela entrada.

A primeira passagem é responsável por dividir o arquivo em linhas, procurando por quebras de linha no arquivo passado e criando uma estrutura em forma de lista encadeada em que cada linha tem um ponteiro para a próxima linha.

A segunda passagem é responsável por expandir \emph{include}s. A inclusão é realizada textualmente, ou seja, o arquivo que é incluído é lido, divido em linhas e o seu conteúdo é inserido no local da inclusão.

A terceira passagem é responsável por dividir todas as linhas em \emph{tokens}. \emph{Tokens} são conjuntos de caracteres que possuem algum significado, como por exemplo: o nome de rótulos, o nome das instruções, números, operadores matemáticos, etc. Os \emph{tokens} também são armazenados em uma lista encadeada.

A quarta passagem é responsável por classificar cada linha de acordo com o seu tipo. Por exemplo, uma linha pode ser do tipo: alocação de variável, declaração de macro, declaração de $equ$, etc.

A quinta passagem é responsável por substituir todos os $equ$s.

A sexta passagem é responsável por fazer a substituição de macros.

A sétima passagem é responsável por calcular o endereço de cada linha.

A oitava passagem é responsável por substituir cada utilização de rótulo por seu devido endereço.

A nona passagem é responsável por resolver expressões matemáticas nos operandos de instruções. Para resolver, a expressão o montador utiliza um \emph{parser} descendente recursivo que implementa uma gramática livre de contexto do tipo LL(1).

A décima passagem é responsável por gerar o código de máquina do programa e armazená-lo em um \emph{buffer}.

Por fim, os arquivos de saída são gerados lendo o \emph{buffer} com o código de máquina.
