\chapter{TESTES REALIZADOS}
\label{chp:testes}

A montagem do CES em quatro \emph{protobards} é um projeto consideravelmente grande. Cada conexão entre dois pinos está sujeita a erro humano. Além disso, outros problemas podem acontecer, como um mau contato nas vias de uma \emph{protoboard} ou um fio partido. Fazer a montagem de todo o projeto para somente depois lidar com os possíveis problemas tornaria a solução destes mais complicada, visto que o problema estaria inserido em um sistema mais complexo, e encontrar a causa raiz do problema demandaria mais tempo.

Uma situação similar é encontrada em Desenvolvimento de \emph{Software}, em que localizar e corrigir um \emph{bug} em um sistema complexo leva mais tempo (e portanto é mais custoso) do que encontrar este problema durante o desenvolvimento dos componentes que formam esse sistema. A Figura \ref{fig:custo_bug} mostra o custo relativo para corrigir um problema nas diversas fases do ciclo de desenvolvimento de um \emph{software}.

\begin{figure}[ht] \centering
    \caption{Custo relativo para corrigir uma falha}
    \includegraphics[width=12.5cm]{NIST-relative-cost-to-fix-a-flaw.png}
    \legend{Fonte: National Institute of Science and Technology (NIST)}
    \label{fig:custo_bug}
\end{figure}

Visando minimizar o custo de correção de falhas, aconselha-se cada vez mais a criação de testes que possam encontrar uma falha logo no início. Duas categorias importantes são:
\begin{itemize}
\item Teste unitário: são testes de baixo nível. Eles consistem em testar métodos e funções individualmente;
\item Teste de integração: certificam-se de que módulos ou serviços usados pela aplicação funcionam bem juntos.
\end{itemize}

Fonte: Atlassian, \cite{url:Atlassian}.

\bigskip

De forma similar aos componentes de um \emph{software}, os componentes do CES foram testados buscando-se encontrar falhas nas fases iniciais do desenvolvimento do projeto.

Para a realização dos testes, foi utilizado um Arduino Mega. Essa placa foi escolhida por dois motivos principais. Primeiro, as placas Arduino possuem uma vasta biblioteca que tornam a sua programação mais fácil do que a programação de um micro-controlador simplesmente. Essa característica permite uma iteração rápida do ciclo que envolve avaliar os requisitos de um teste, programar este teste e rodá-lo no circuito em questão. Segundo, o Arduino Mega utiliza como base o micro-controlador ATmega2560 e expõe 70 pinos de entrada/saída do micro-controlador. Esse grande número de pinos é essencial, pois o CES possui barramentos de 16 \emph{bits}.

A escrita e leitura direta das portas do micro-controlador foram utilizadas nos testes para evitar dependência nas rotinas $digitalWrite$ e $digitalRead$ da biblioteca do Arduino, que são conhecidamente lentas, e para evitar manipulação direta de \emph{bits} (e em vez disso utilizar manipulação de \emph{bytes}). As informações sobre as portas do micro-controlador, assim como outros aspectos deste, foram encontradas no datasheet fornecido pela Atmel e que pode ser encontrado em \cite{url:ATmega2560}.

Para relacionar as portas do ATmega2560 com os pinos no Arduino Mega, foi utilizada a Figura \ref{fig:arduino_mega_pinout}.

\begin{figure}[ht] \centering
    \caption{Pinos do Arduino Mega}
    \includegraphics[width=12.5cm]{Arduino_Mega_Pinout.png}
    \legend{Fonte:  \cite{url:ArduinoMegaPinout}}
    \label{fig:arduino_mega_pinout}
\end{figure}

Os testes foram capazes de detectar diversos erros como maus contatos, fios partidos, conexões erradas e circuitos integrados com defeito. Realizar esses testes antes de ter o projeto completo foi muito importante, pois detectar alguns desses erros em um sistema complexo demandaria muito tempo.

Os Códigos \ref{cod:test_placa1}, \ref{cod:test_placa2_BD_BT}, \ref{cod:test_placa2_ME_RE}, \ref{cod:test_placa2_BE}, \ref{cod:test_placa1_e_2} e \ref{cod:test_placa3} correspondem a cada teste realizado e podem ser encontrados no Apêndice \ref{appendix:tests}. A seguir será descrito quais foram os critérios para cada teste realizado.

\clearpage

\section{Teste da placa 1}
\label{sec:teste_placa_1}

Este teste encontra-se no Código \ref{cod:test_placa1}.

Em todos os testes realizados, buscou-se começar o teste dependendo do menor número possível de componentes, pois dessa forma seria fácil encontrar problemas que estivessem ocorrendo diretamente na saída da placa. Dessa forma, o primeiro teste a ser realizado na placa 1 é inserir $0$ nas Chaves de Dados, passando esses dados diretamente pelo somador (somando com o valor $0$) e lendo o valor na saída do somador. Em seguida são testados todos os demais valores possíveis nas Chaves de Dados, ou seja, de $0$ até $2^{16} - 1$, também somando com o valor $0$ e lendo o valor na saída do somador. Este é um teste parcial do somador e dos buffers de entrada das Chaves de Dados.

Em seguida, este teste é repetido, mas dessa vez \textbf{Mais1} é ativado. Dessa forma, espera-se encontrar na saída do somador o valor inserido acrescido de $1$. Este é mais um teste parcial dos somadores.

Depois é realizado um teste similar aos dois primeiros, porém, agora o valor não é inserido nas Chaves de Dados, em vez disso o valor é inserido no barramento de dados, armazenado no registrador RD e direcionado para o somador para ser somado com o valor $0$. Este é um teste completo de registrador RD. Note que o primeiro teste garante que se esse teste falhar o problema deve estar em RD.

Logo após, o registrador RP é testado utilizando uma lógica similar à que ocorre quando o CES está em funcionamento. Primeiro obtém-se o valor $0$ na saída do somador utilizando o mesmo método do primeiro teste. Esse valor é então escrito no registrador RP. A entrada pelas Chaves de Dados é desabilitada, e a nova entrada esquerda do somador passa a ser o registrador RP. No lado direito do somador está o valor $0$, e \textbf{Mais1} é ativado. Dessa forma, espera-se encontrar o valor $1$ na saída do somador. Essa valor é então escrito em RP, e agora espera-se pelo valor $2$ na saída do somador. Essa lógica é repetida para todos os valores possíveis, ou seja, até $2^{14} - 1$, dado que RP é um registrador de 14 \emph{bits}. Este é um teste completo do registrador RP.

Um teste similar ao anterior é realizado em RT. Porém, o valor de RT só pode ser acessado após passar pelo complementador (ou seja, o valor de RT invertido). Para isso, escreve-se no barramento de dados o valor a ser testado, começando por $0$. Este valor é armazenado em RD e depois é levado para a saída do somador. O valor é então lido para o registrador RT ao mesmo tempo que o valor $0$ é lido para o registrador RD. É realizada uma soma com o valor $0$ na entrada esquerda do somador, o inverso do valor sendo testado (valor em RT) na entrada direita e \textbf{Mais1} é ativado. O valor esperado na saída do somador é o resultado de $0 - RT$. Este teste é repetido para todos os valores possíveis, ou seja, de $0$ até $2^{16} - 1$. Com isso testa-se RT, o complementador e parcialmente o somador. Esse não é um teste ideal, pois este depende de diversos componentes, o que dificulta a identificação da origem de uma possível falha. Porém, dado que o valor de RT não pode ser acessado diretamente, esse é o melhor possível nessa situação.

Por fim, testou-se completamente o somador. O teste ideal do somador consiste em executar todas as somas possíveis e verificar o resultado, porém, esse não é um teste viável dado que o número total de iterações seria $2^{32}$. Em vez disso testou-se individualmente os quatro somadores de 4 \emph{bits} que formam o somador da seguinte forma. O primeiro somador é testado somando todos os números de $0$ a $16$ do lado esquerdo com todos os números de $0$ a $16$ do lado direito. O mesmo teste é repetido, mas dessa vez fazendo um \emph{shift} de 4 \emph{bits} para a esquerda em ambos os valores, assim testando o segundo dos quatro somadores. Essa lógica é repetida mais duas vezes para testar completamente todos os somadores.

\section{Teste da placa 2}

A segunda placa possui o maior número de conexões externas (conexões para outras placas). Por esse motivo, nem mesmo utilizando os 70 pinos disponíveis no Arduino Mega foi possível testar a placa por completo em um único teste. Foram realizados 3 testes diferentes na placa 2, cada um testando um conjunto lógico diferente da placa. Primeiro testou-se  Buffer das Chaves de Dados ($BD$) e o Buffer do Registrador de Trabalho ($BT$). O segundo teste envolveu o Multiplexador de Endereços ($ME$) e o Registrador de Endereços ($RE$). Por fim, testou-se o Buffer das Chaves de Endereços. Vale notar que o Buffer dos LEDs ($BL$) não foi testado, pois o funcionamento deste não é essencial para o correto funcionamento do computador.

\subsection{Teste de BD e BT}
\label{subsec:teste_bd_bt}

Este teste encontra-se no Código \ref{cod:test_placa2_BD_BT}.

Ambos os \emph{buffers} tem como saída as Vias de Dados do Barramento (VDB), portanto esse teste sempre lê VDB para obter o resultado.

\emph{Buffers} possuem uma função lógica relativamente simples, e por isso os seus testes também são simples. Para testar o \emph{Buffer} de Dados, são escritos nas Chaves de Dados (entrada de BD) todos os valores de $0$ a $2^{16}-1$ e a resposta é lida em VDB.

Para o teste de $BT$ a mesma lógica é repetida. Todos os valores de $0$ a $2^{16}-1$ são escritos na entrada de BT, e o resultado é lido em VDB.

\subsection{Teste de ME e RE}

Este teste encontra-se no Código \ref{cod:test_placa2_ME_RE}.

Esses dois componentes precisam ser testados ao mesmo tempo, pois a saída de ME não pode ser acessada diretamente, em vez disso, a saída de ME é utilizada como entrada de RE. O conjunto testado é formado por duas entradas (a Via de Dados do Barramento (VDB) e a Saída do Somador (SS)) e uma saída (a Via de Endereços do Barramento(VEB)).

O teste é realizado da seguinte forma. Todos os valores de $0$ a $2^{16}-1$ são escritos na entrada $A$ do multiplexador, ou seja, em VDB. Ao mesmo tempo o complemento desse valor (sendo o valor em uma dada iteração igual a $i$ o complemento é calculado obtendo com o resultado de $0xFFFF - i$) é escrito na entrada $B$ do multiplexador, ou seja, na Saída do Somador (SS). Esses valores são escolhidos para manter a maior distância de Hamming entre eles.

Em cada iteração, primeiro seleciona-se a entrada $A$ do multiplexador, verificando se o valor correto é obtido em VEB. Em seguida, seleciona-se a entrada $B$ do multiplexador e verifica-se o valor em VEB.

\subsection{Teste de BE}

Este teste encontra-se no Código \ref{cod:test_placa2_BE}.

Como BE também é um \emph{buffer}, este teste é similar ao teste em \ref{subsec:teste_bd_bt}. Sua entrada são as Chaves de Endereço e sua saída é a Via de Endereço do Barramento (VEB). Todos os valores possíveis, isto é, de $0$ a $2^{14}-1$, são escritos nas Chaves de Endereços, e o resultado é lido em VEB.

\section{Teste de integração das placas 1 e 2}
\label{sec:teste_1_e_2}

É comum em uma arquitetura de Von Neumann a divisão do processador em Unidade de Controle, Unidade Lógica e Aritmética e Registradores. De forma geral, a UC é responsável por gerar sinais que controlam os Registradores e a ALU. A UC do CES está totalmente contida na placa 3, enquanto os Registradores e a ALU estão distribuídos nas placas 1 e 2. Dessa forma, faz sentido integrar essas duas placas e testar o funcionamento das duas em conjunto, simulando os sinais da UC.

Este teste encontra-se no Código \ref{cod:test_placa1_e_2} e pode ser dividido em 4 partes.

\subsection{Teste das chaves}

Neste teste os \emph{buffers} das Chaves de Dados e das Chaves de Endereço são testados. Além disso, a estabilidade do valor das chaves também é testada da seguinte forma: em cada iteração o valor do resultado é lido 255 vezes antes de avançar para a próxima iteração. Se pelo menos um desses 255 não corresponder ao valor esperado, o teste falha.

Para as Chaves de Endereço, o valor do resultado é lido nas Vias de Endereços do Barramento. Os valores testados variam de $0$ a $2^{14}-1$.

Para as Chaves de Dados o valor do resultado é lido em dois locais diferentes e em duas etapas. Primeiro, o resultado é lido diretamente das Vias de Dados do Barramento, utilizando como saída o \emph{buffer} BD.

Em seguida, o resultado é lido também em VDB, mas dessa vez como uma forma de ler indiretamente o valor de RT. O valor testado é escrito nas Chaves de Dados, passa pelo \emph{buffer} BS, é somado ao valor zero e é armazenado em RT. Finalmente, utiliza-se o \emph{buffer} BT para emitir o valor de RT nas Vias de Dados do Barramento.

\subsection{Teste de incremento de RP}

Este teste é similar ao descrito na Seção \ref{sec:teste_placa_1}. O valor $0$ é carregado em RP. Em seguida, é executado um \emph{loop} em que o valor de RP é somado ao valor $0$, e com o sinal \textbf{Mais1} é ativado. O valor da soma é armazenado novamente em RP. Este teste itera sobre todos os valores possíveis de RP, isto é, de $0$ a $2^{14}-1$.

\subsection{Teste de RD}

Nesse teste o Registrador de Dados é testado com todos os valores possíveis, isto é, de $0$ a $2^{16}-1$. Cada valor é escrito nas Vias de Dados do Barramento e armazenado em RD. Em seguida, o valor é levado à entrada esquerda do somador. Na entrada direita está o valor $0$. O sinal \textbf{Mais1} pode estar ativado ou desativado com probabilidade de $50\%$. O valor da soma é armazenado em RT e depois é escrito na Via de Dados do Barramento na qual é lido e comparado com o valor esperado.

\subsection{Teste de RT}

Este é um teste bem completo de todos os componentes das duas placas. Nele itera-se sobre todos os valores possíveis para o Registrador de Trabalho (RT), ou seja, de $0$ a $2^{16}-1$. Primeiro, o valor a ser testado é invertido (inversão \emph{bit}-a-\emph{bit}) e escrito nas Vias de Dados do Barramento. Esse valor passa por RD e é levado à entrada esquerda do somador. Na entrada direita do somador está o valor $0$, e \textbf{Mais1} está desativado. O valor sai do somador sem alteração e é armazenado em RT. Agora o valor $0$ é inserido em RD e levado à entrada esquerda do somador. Na entrada direita é inserido o valor de RT após passar pelo Complementador de Dados. O resultado (que deve ser o valor sendo testado, sem inversão) é novamente armazenado em RT, mas dessa vez utiliza-se o \emph{buffer} BT para emitir esse valor na Via de Dados do Barramento, na qual ele é finalmente lido e comparado ao valor a ser testado.

\section{Teste da placa 3}

Este teste encontra-se no Código \ref{cod:test_placa3}.

Como mencionado na Seção \ref{sec:teste_1_e_2}, a placa 3 é composta primariamente pela Unidade de Controle. Pela Imagem \ref{fig:nova_uc} podemos notar que a lógica da UC é em sua maior parte combinacional, portanto o teste mais apropriado para esta placa é um teste de Tabela Verdade, em que todas as possíveis entradas são exercitadas, e o valor de cada uma das saídas é verificado. A Tabela \ref{tab:novos_sinais_controle} mostra as expressões lógicas de todos os sinais gerados pela UC.

A UC possui 13 variáveis de entrada e 25 sinais de saída. As variáveis de entrada são: $I_0$, $I_1$, $RC$, $Para$, $Est_1$, $Parte$, $MudaP$, $MudaT$, $BotParte$, $Partiu$, $IntPara$, $BotEscMem$ e $\overline{EntRlg}$.

E os sinais de saída são: $Desvia$, $EscM$, $UltC$, $EscP$, $EscT$, $EscC$, $ZComp$, \textbf{Mais1}, $EntRlg$, $\overline{Rlg}$, $Rlg1$, $Rlg2$, $Rlg3$, $BotParteAtivo$, $\overline{BotParteAtivo}$, $VaiPartir$, $VaiParar$, $EscMem$, $SaiBD$, $SaiBE$, $SaiBS$, $SaiRE$, $SaiRP$, $SaiRI$ e $SaiRC$.

Alguns dos valores de entrada não possuem conexão externa e são, em vez disso, gerados por \emph{flip-flops}. Nesses casos, fez-se com que os \emph{flip-flops} emitissem os valores desejados da seguinte forma: para \emph{flip-flops} do tipo D o valor desejado é inserido diretamente na sua entrada D, e um ciclo de \emph{clock} é gerado para fazê-lo admitir o valor. \emph{Flip-flops} do tipo JK foram momentaneamente conectados de forma a se comportarem como um \emph{flip-flop} do tipo D, isto é, o valor desejado foi inserido na entrada J e inverso desse valor na entrada K.

Como a UC possui 13 variáveis de entrada, a sua Tabela Verdade possui $2^{13}$ linhas. Cada uma dessas linhas foi testada, e o valor das 25 variáveis foi lido e comparado com o esperado. O valor esperado foi calculado pelo micro-controlador utilizando as expressões lógicas da Tabela \ref{tab:novos_sinais_controle}, pois dessa forma não foi necessário pré-computar e armazenar todas as $8096$ linhas da tabela.

\section{Ausência do teste da placa 4}

Os únicos componentes presentes na quarta placa e que são fundamentais para o funcionamento do computador são as memórias (RAM e ROM) e o Decodificador de Endereços. Não existe um bom método para testar as memórias, visto que cada pastilha de memória é um Circuito Integrado sem acesso ao seu funcionamento. Além disso, cada pastilha de memória pode ser simplesmente testada utilizando a função de teste de um gravador de CIs.

Quanto ao Decodificador de Endereços, embora possa ser criado um teste para avaliá-lo, o teste seria muito simples, e considerando o tempo necessário para escrever e preparar este teste com os possíveis benefícios deste, julgou-se desnecessário este teste.
