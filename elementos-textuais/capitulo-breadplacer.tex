\chapter{FERRAMENTA DE DISPOSIÇÃO DOS CIRCUITOS INTEGRADOS}
\label{chp:breadplacer}

Para trabalhar com um número grande de circuitos integrados em 4 \emph{protoboards}, viu-se necessário uma ferramenta para dispor os CIs nas placas, de forma a criar um diagrama que pudesse ser utilizado para posicionar os CIs nas placas e conectá-los.

De início um editor de texto foi utilizado, mas este se mostrou inadequado para a tarefa. Pequenas modificações, como mover um CI, significavam ter que refazer uma grande parte do documento, o que tornou impossível iterações rápidas do diagrama. Portanto, tornou-se necessário o uso de uma ferramenta especializada para tal tarefa.

Ferramentas para a criação de diagramas lógicos de circuitos e até mesmo de placas de circuito impresso estão disponíveis, mas uma ferramenta para a criação de diagramas de \emph{protoboards} não foi encontrada. Por essa razão, uma ferramenta para a criação desse tipo de diagrama foi desenvolvida para o projeto. A ferramenta em questão foi nomeada \emph{bread\_placer} (pois em inglês \emph{protoboards} são chamadas de \emph{breadboards}) e o seu código pode ser encontrado em: \cite{url:Breadplacer}. Os diagramas mostrados no capítulo 4 foram criados utilizando essa ferramenta.

Para o desenvolvimento da ferramenta, foi utilizada a biblioteca \emph{SDL}, que provê abstrações para geração de imagens gráficas. Esta biblioteca é comumente utilizada para desenvolvimento de jogos e por isso está disponível em diversas plataformas. Com isso, a ferramenta \emph{bread\_placer} pode, em teoria, ser portada sem muito esforço para qualquer plataforma que disponibilize a biblioteca padrão da linguagem C e a biblioteca \emph{SDL}. Para o desenvolvimento desse trabalho, a ferramenta foi utilizada somente em ambiente \emph{Linux}.

A entrada da ferramenta se dá através de um arquivo de extensão \emph{ics\_list} que possui informação sobre todos os CIs que devem ser colocados na placa. Sempre que a ferramenta é fechada, um arquivo de extensão \emph{icprj} com o mesmo nome do arquivo de entrada é gerado com informação sobre a disposição dos CIs na placa. Esse arquivo deve ser passado para a ferramenta nas execuções posteriores, pois o arquivo de extensão \emph{ics\_list} é lido automaticamente quando um arquivo \emph{icprj} é utilizado. Mesmo após a primeira execução, mais CIs podem ser adicionados ao arquivo \emph{ics\_list}, e estes serão automaticamente incluídos no projeto como CIs fora da placa.

\section{Sintaxe do arquivo \emph{ics\_list}}

Este arquivo deve conter um ou mais CIs. Componentes são descritos por grupos de linhas separados por uma linha em branco. Quando uma linha em branco ou o final do arquivo é encontrado, a ferramenta termina a descrição do CI atual. Um exemplo da sintaxe desse arquivo encontra-se no Código \ref{cod:icslist}.

\lstinputlisting[style=customC,caption={Exemplo \emph{ics\_list}},label=cod:icslist]{codigos/Exemplo_icslist.txt}

A primeira linha de cada bloco começa com as letras $IC$ seguidas do número de pinos que o CI possui. As duas próximas linhas contêm as palavras $Code$ e $Name$ e devem ser seguidas, respectivamente, do código do CI e do seu nome no diagrama. Logo em seguida há uma linha com apenas a palavra $Pins$, após ela os pinos do CI devem ser descritos. A ordem com que os pinos são descritos não é importante, mas todos os pinos devem ser descritos antes do final do bloco.

A descrição de um pino se dá da seguinte maneira:

\begin{itemize}
\item Primeiro um símbolo: $\#$ indica que esse pino se conecta a outra placa e $*$ indica que todas as conexões desse pino estão dentro da mesma placa. Essa informação não é verificada pela ferramenta no momento, porém, o símbolo $\#$ é utilizado para deixar a etiqueta do pino em negrito;
\item Após o símbolo deve vir o número do pino dentro do CI. Essa numeração começa em 1;
\item Por fim deve vir o nome, ou etiqueta, do pino. Existem valores especiais para esse campo: VCC(...) deixará a etiqueta vermelha, GND(...) deixará a etiqueta cinza e N.C. (ou seja, Não Conectado) deixará a etiqueta azul.
\end{itemize}

\section{Utilizando a ferramenta}
A ferramenta divide conceitualmente os CIs em dois grupos. Os que estão na placa e os que estão fora dela. Do lado superior direito da tela há um contador de quantos CIs estão fora da placa. Ao iniciar a ferramenta pela primeira vez, todos os CIs estarão fora da placa.

Além disso, para os CIs que estão na placa existe o conceito de selecionado ou não. Apenas um CI pode estar selecionado em dado momento, e somente um CI selecionado pode ser movido.

A ferramenta possui um ``cursor'' que é colorido de azul quando não há CI selecionado e de roxo quando há um CI selecionado.

Todos os comandos da ferramenta são executados com o teclado e são os seguintes.

\begin{itemize}
\item $Q$ ou $ESC$: Fecha a ferramenta;
\item $Z$: Entra ou sai do modo de zoom;
\item $W$, $A$, $S$ e $D$: Movem a janela quando em modo de zoom;
\item \emph{Setas direcionais}: Movem o cursor. Quando há um CI selecionado, este é movido junto do cursor;
\item $R$: Se houver um CI selecionado, este é rotacionado 180º;
\item $BACKSPACE$ ou $DELETE$: Se um CI estiver selecionado, leva este CI para fora da placa;
\item $I$: Insere na placa um CI de fora dela. Uma janela é aberta para escolher o CI desejado;
\item $SPACEBAR$ ou $ENTER$: Seleciona o CI que estiver sob o cursor;
\end{itemize}

Dois CIs não podem ocupar o mesmo espaço. Por isso, sempre que um CI estiver sendo movido, deve haver espaço para movê-lo. Da mesma forma, para inserir na placa um CI de fora da placa também deve haver espaço suficiente para o CI ser inserido.

\section{Formatos de saída}

A ferramenta tem como saída uma imagem com o diagrama idêntico ao visualizado durante a sua utilização no formato \emph{bmp}. Além disso, em ambiente \emph{Linux}, foi testado gerar uma imagem vetorial em formato \emph{svg} utilizando \emph{scripts} em \emph{bash} e em \emph{python} e os comandos \emph{convert} (parte do \emph{ImageMagick}) e \emph{potrace}.
