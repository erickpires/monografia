\changetocdepth {4}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {INTRODU\IeC {\c C}\IeC {\~A}O}}{15}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o e objetivos}{15}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Trabalhos relacionados}{15}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Gigatron}{15}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}Ben Eater}{16}{subsection.1.2.2}% 
\contentsline {subsection}{\numberline {1.2.3}dreamcatcher}{16}{subsection.1.2.3}% 
\contentsline {section}{\numberline {1.3}Estrutura da monografia}{16}{section.1.3}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {CES: COMPUTADOR EXTREMAMENTE SIMPLES}}{17}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Instru\IeC {\c c}\IeC {\~o}es}{17}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Registradores}{17}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Unidade L\IeC {\'o}gica e Aritm\IeC {\'e}tica}{18}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Execu\IeC {\c c}\IeC {\~a}o das instru\IeC {\c c}\IeC {\~o}es}{18}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Arquitetura do CES}{19}{section.2.5}% 
\contentsline {section}{\numberline {2.6}Unidade de Controle}{20}{section.2.6}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {MODIFICA\IeC {\c C}\IeC {\~O}ES PARA ACOMODAR O PAINEL E A PARADA DO PROCESSADOR}}{24}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Sobre o painel do simulador}{24}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Sobre o painel projetado para o CES}{25}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Diferen\IeC {\c c}as entre os pain\IeC {\'e}is}{26}{subsection.3.2.1}% 
\contentsline {section}{\numberline {3.3}Altera\IeC {\c c}\IeC {\~o}es na Arquitetura do CES}{27}{section.3.3}% 
\contentsline {section}{\numberline {3.4}A parada do processador}{27}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}Pseudo instru\IeC {\c c}\IeC {\~a}o de partida}{29}{subsection.3.4.1}% 
\contentsline {section}{\numberline {3.5}Altera\IeC {\c c}\IeC {\~o}es na Unidade de Controle}{29}{section.3.5}% 
\contentsline {section}{\numberline {3.6}Outras modifica\IeC {\c c}\IeC {\~o}es realizadas no CES}{33}{section.3.6}% 
\contentsline {subsection}{\numberline {3.6.1}O \emph {buffer} dos \emph {LEDs}}{33}{subsection.3.6.1}% 
\contentsline {subsection}{\numberline {3.6.2}Sobre o \emph {clock} do processador}{33}{subsection.3.6.2}% 
\contentsline {subsection}{\numberline {3.6.3}Decodifica\IeC {\c c}\IeC {\~a}o de Endere\IeC {\c c}o}{34}{subsection.3.6.3}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {DIVIS\IeC {\~A}O EM PLACAS}}{36}{chapter.4}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {ESCOLHA DO HARDWARE}}{42}{chapter.5}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {MONTAGEM DO COMPUTADOR}}{44}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Conex\IeC {\~o}es internas}{44}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Conex\IeC {\~o}es externas}{45}{section.6.2}% 
\contentsline {section}{\numberline {6.3}Fonte de alimenta\IeC {\c c}\IeC {\~a}o}{45}{section.6.3}% 
\contentsline {section}{\numberline {6.4}Resultado final}{46}{section.6.4}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {TESTES REALIZADOS}}{47}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Teste da placa 1}{50}{section.7.1}% 
\contentsline {section}{\numberline {7.2}Teste da placa 2}{51}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}Teste de BD e BT}{51}{subsection.7.2.1}% 
\contentsline {subsection}{\numberline {7.2.2}Teste de ME e RE}{51}{subsection.7.2.2}% 
\contentsline {subsection}{\numberline {7.2.3}Teste de BE}{52}{subsection.7.2.3}% 
\contentsline {section}{\numberline {7.3}Teste de integra\IeC {\c c}\IeC {\~a}o das placas 1 e 2}{52}{section.7.3}% 
\contentsline {subsection}{\numberline {7.3.1}Teste das chaves}{52}{subsection.7.3.1}% 
\contentsline {subsection}{\numberline {7.3.2}Teste de incremento de RP}{53}{subsection.7.3.2}% 
\contentsline {subsection}{\numberline {7.3.3}Teste de RD}{53}{subsection.7.3.3}% 
\contentsline {subsection}{\numberline {7.3.4}Teste de RT}{53}{subsection.7.3.4}% 
\contentsline {section}{\numberline {7.4}Teste da placa 3}{53}{section.7.4}% 
\contentsline {section}{\numberline {7.5}Aus\IeC {\^e}ncia do teste da placa 4}{54}{section.7.5}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {8}\MakeTextUppercase {FERRAMENTA DE DISPOSI\IeC {\c C}\IeC {\~A}O DOS CIRCUITOS INTEGRADOS}}{55}{chapter.8}% 
\contentsline {section}{\numberline {8.1}Sintaxe do arquivo \emph {ics\_list}}{55}{section.8.1}% 
\contentsline {section}{\numberline {8.2}Utilizando a ferramenta}{57}{section.8.2}% 
\contentsline {section}{\numberline {8.3}Formatos de sa\IeC {\'\i }da}{57}{section.8.3}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {9}\MakeTextUppercase {UM MONTADOR EXTREMAMENTE SIMPLES}}{58}{chapter.9}% 
\contentsline {section}{\numberline {9.1}Sobre o montador}{58}{section.9.1}% 
\contentsline {section}{\numberline {9.2}Interface da linha de comando}{63}{section.9.2}% 
\contentsline {section}{\numberline {9.3}Opera\IeC {\c c}\IeC {\~a}o do montador}{64}{section.9.3}% 
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {10}\MakeTextUppercase {CONCLUS\IeC {\~A}O}}{65}{chapter.10}% 
\contentsline {section}{\numberline {10.1}VIS\IeC {\~A}O GERAL}{65}{section.10.1}% 
\contentsline {section}{\numberline {10.2}DIFICULDADES ENCONTRADAS}{65}{section.10.2}% 
\contentsline {section}{\numberline {10.3}TRABALHOS FUTUROS}{66}{section.10.3}% 
\contentsline {subsection}{\numberline {10.3.1}Interface de Entrada e Sa\IeC {\'\i }da}{66}{subsection.10.3.1}% 
\contentsline {subsection}{\numberline {10.3.2}Placa de Circuito Impresso}{66}{subsection.10.3.2}% 
\contentsline {section}{\numberline {10.4}OBSERVA\IeC {\c C}\IeC {\~O}ES FINAIS}{67}{section.10.4}% 
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\MakeTextUppercase {Refer\^encias}}{68}{section*.11}% 
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {\textnormal {Teste das Placas}}}{70}{appendix.A}% 
